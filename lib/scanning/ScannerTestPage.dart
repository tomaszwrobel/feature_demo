import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

class ScannerTestPage extends StatefulWidget {

  @override
  ScannerState createState() => ScannerState();

}

class ScannerState extends State<ScannerTestPage> {
  String _text = "Scan code ..";

  @override
  initState(){
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Scan QR code')),
      // Wait until the controller is initialized before displaying the
      // camera preview. Use a FutureBuilder to display a loading spinner
      // until the controller has finished initializing.
      body: Container(
        child: Center(
          child: Text(_text),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.scanner),
        // Provide an onPressed callback.
        onPressed: () => scan(),
      ),
    );
  }

  Future scan() async {
    try {
      String barcode = await FlutterBarcodeScanner
          .scanBarcode("#ff6666", "Cancel", true, ScanMode.DEFAULT);
      setState(() => this._text = barcode);
    } catch (e) {
      setState(() {
        this._text = 'Error scanning code';
      });
    }
  }
}
