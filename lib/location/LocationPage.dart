import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:latlong/latlong.dart';
import 'package:location/location.dart';

class LocationPage extends StatefulWidget {
  LocationPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LocationPageState createState() => _LocationPageState();
}

class _LocationPageState extends State<LocationPage> {
  LatLng _markerLatLng;
  MapController _mapController;

  @override
  void initState() {
    super.initState();
    _markerLatLng = new LatLng(51.5, -0.09);
    _mapController = MapController();
    getCurrentLocation().then((loc) => _setMarker(loc));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Current location demo"),
        ),
        body: FlutterMap(
          mapController: _mapController,
          options: new MapOptions(
            center: _markerLatLng,
            zoom: 13.0,
          ),
          layers: [
            new TileLayerOptions(
              urlTemplate: "http://a.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png",
            ),
            new MarkerLayerOptions(
              markers: [
                new Marker(
                  point: _markerLatLng,
                  builder: (ctx) => new Container(
                    child: new Icon(Icons.directions_walk),
                  ),
                ),
              ],
            ),
          ],
        ));
  }

  _setMarker(LocationData loc){
    setState(() => updateCenter(loc));
  }
  updateCenter(LocationData loc){
    _markerLatLng = LatLng(loc.latitude, loc.longitude);
    _mapController.onReady.then((p) => _mapController.move(_markerLatLng, 15.0));
  }
  Future<LocationData> getCurrentLocation() async {
    var location = new Location();
    try {
      return location.getLocation();
    } catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        debugPrint('Permission denied');
      }
      return null;
    }
  }
}
